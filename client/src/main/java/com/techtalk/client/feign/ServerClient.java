package com.techtalk.client.feign;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;

@FeignClient(name = "server", url = "http://localhost:8081")
public interface ServerClient {
    @GetMapping("/hello")
    String getServerResponse();
}