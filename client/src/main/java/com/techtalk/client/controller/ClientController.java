package com.techtalk.client.controller;

import com.techtalk.client.service.ServerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ClientController {
    @Autowired
    private ServerService serverService;

    @GetMapping("/invoke-server")
    public String invokeServer() {
        return serverService.getServerResponse();
    }
}
