package com.techtalk.client.service;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.techtalk.client.feign.ServerClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ServerService {
    @Autowired
    private ServerClient serverClient;

    @HystrixCommand(fallbackMethod = "fallbackResponse")
    public String getServerResponse() {
        return serverClient.getServerResponse();
    }

    public String fallbackResponse() {
        return "Fallback Response - Server is down";
    }
}